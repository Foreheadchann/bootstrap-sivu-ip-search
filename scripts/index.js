const queryButton = document.querySelector('button#ip-query');
const darkmodeToggle = document.querySelector('input#darkmode-switch');

const constructUrl = function(ip) {
    return `https://ipapi.co/${ip}/json/`;
}

const responseTestUrl = constructUrl('127.0.0.1');

/* https://www.oreilly.com/library/view/regular-expressions-cookbook/9780596802837/ch07s16.html */
const validateIPAddress = function(ip) {
    return /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip);
}

/* https://stackoverflow.com/questions/247483/http-get-request-in-javascript */
const httpGetAsync = function(url, callback) {
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            callback(xmlHttp.responseText);
        }
    }
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

const constructDataTableHead = function() {
    const thead = document.createElement('thead');
    const tr = document.createElement('tr');

    const key = document.createElement('th');
    key.appendChild(document.createTextNode('Key'));
    key.setAttribute('scope', 'col');

    const value = document.createElement('th');
    value.appendChild(document.createTextNode('Value'));
    value.setAttribute('scope', 'col');

    tr.appendChild(key);
    tr.appendChild(value);
    thead.appendChild(tr);

    return thead;
}

const constructDataTableBody = function(json) {
    const tbody = document.createElement('tbody');
    
    JSON.parse(json, (key, value) => {
        if(key != '') {
            const tr = document.createElement('tr');
            const keyElement = document.createElement('th');
            const valueElement = document.createElement('td');

            keyElement.appendChild(document.createTextNode(key));
            keyElement.setAttribute('scope', 'row');
            keyElement.setAttribute('class', 'reduced-font-weight');

            valueElement.appendChild(document.createTextNode(value));

            tr.appendChild(keyElement);
            tr.appendChild(valueElement);
            tbody.appendChild(tr);
        }
    });

    return tbody;
}

const setupDarkmode = function() {
    const dataContainer = document.querySelector('div.data-container');
    const table = dataContainer.querySelector('table#removable-table');
    const toggled = darkmodeToggle.checked;
    const root = document.documentElement;
    
    if(toggled) {
        root.style.setProperty('--body-background-color', '#202025'); /* Use 25 as Blue instead of 20 due to same values being yellowish */
        root.style.setProperty('--body-text-color', '#CCCCCC');
        root.style.setProperty('--body-small-text-color', '#999999');

        if(table != null && table != undefined && !table.classList.contains('table-dark')) {
            table.classList.add('table-dark');
        }
    } else {
        root.style.setProperty('--body-background-color', '#FFFFFF');
        root.style.setProperty('--body-text-color', '#000000');
        root.style.setProperty('--body-small-text-color', '#A0A0A0');

        if(table != null && table != undefined && table.classList.contains('table-dark')) {
            table.classList.remove('table-dark');
        }
    }
}

window.onload = function () {
    setupDarkmode();

    console.log('[Query Manager] Testing Connection to https://ipapi.co/');

    const start = Date.now();

    httpGetAsync(responseTestUrl, function() {
        const responseTime = Date.now() - start;
        console.log(`[Query Manager] Received Response in ${responseTime}ms`);
        document.querySelector('small#response-time').setAttribute('display-data', responseTime);
    });
}

queryButton.onclick = function() {
    const ipAddress = document.querySelector('input#ip-input').value;
    const resultInfo = document.querySelector('small#result-info');
    const queryUrl = constructUrl(ipAddress);

    resultInfo.setAttribute('display-data', 'Validating Address...');

    console.log(`[Query Manager] Validating IP Address ${ipAddress}`);

    if(validateIPAddress(ipAddress) === true) {
        console.log(`[Query Manager] Address Valid, Getting Data from ${queryUrl}`);

        resultInfo.setAttribute('display-data', 'Getting Data...');

        const start = Date.now();

        httpGetAsync(queryUrl, function(response) {
            const responseTime = Date.now() - start;

            console.log(`[Query Manager] Received Response in ${responseTime}ms`);
            document.querySelector('small#response-time').setAttribute('display-data', responseTime);
            resultInfo.setAttribute('display-data', 'Parsing Response...');

            const dataContainer = document.querySelector('div.data-container');
            const oldTable = dataContainer.querySelector('table#removable-table');

            const table = document.createElement('table');
            const thead = constructDataTableHead();
            const tbody = constructDataTableBody(response);
            
            table.classList.add('table');

            if(darkmodeToggle.checked) {
                table.classList.add('table-dark');
            }

            table.classList.add('table-hover');
            table.classList.add('table-striped');

            table.appendChild(thead);
            table.appendChild(tbody);
            table.setAttribute('id', 'removable-table');

            if(oldTable != null && oldTable != undefined) {
                dataContainer.removeChild(oldTable);
            } 

            dataContainer.appendChild(table);

            resultInfo.setAttribute('display-data', 'Completed Query');
        });

        return;
    }

    console.log('[Query Manager] Address Invalid, Regex Match Failed!');

    resultInfo.setAttribute('display-data', 'Invalid IP Address!');

    setTimeout(function() {
        resultInfo.setAttribute('display-data', 'Waiting...');
    }, 2500);
}

darkmodeToggle.onclick = setupDarkmode;